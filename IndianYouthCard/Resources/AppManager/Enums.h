//
//  Enums.h
//  MCare
//
//  Created by Arbab Khan on 02/09/15.
//  Copyright (c) 2015 Neha. All rights reserved.
//

#ifndef MCare_Enums_h
#define MCare_Enums_h

//==========================NetworkClass=======================//
typedef enum
{
	TASK_LOGIN,
	TASK_SIGNUP,
	TASK_VERIFY_MOBILE,
	TASK_TO_UPDATE_DETAILS
}CURRENT_TASK;
//===========================================================//


//=======================MoreViewController====================//
typedef enum
{
	eAlarm  = 0,
	eReport = 1,
	eNotification = 0,
	eProfile = 1
}enRowType;
//===========================================================//


//=======================ReportViewController====================//
typedef enum
{
	eReportBtnSelectedNone  = 0,
	eReportMonthlyBtnSelected,
	eReportYearlyBtnSelected
}enReportBtnSelected;
//===========================================================//


#endif
