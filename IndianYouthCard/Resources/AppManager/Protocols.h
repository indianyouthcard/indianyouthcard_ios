//
//  Protocols.h
//  MCare
//
//  Created by Arbab Khan on 28/08/15.
//  Copyright (c) 2015 Neha. All rights reserved.
//

#ifndef MCare_Protocols_h
#define MCare_Protocols_h

//====================HistoryTableCell======================//

@protocol HistoryTableCellDelegate <NSObject>

@optional

- (void)btnChat:(NSIndexPath *)inIndexPath;

@end

//======================================================================//
#endif
