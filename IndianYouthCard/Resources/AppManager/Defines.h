
#define kLoginSuccessfulNotificationName    @"LoginSuccessful"
#define kLogoutSuccessfulNotificationName  @"LogoutSuccessful"
#define NULLVALUE(m) (m == [NSNULL null]? @"":m)

#define SUCCESS                                     @"status"
#define DATA                                           @"data"
#define kAlertTitle                                   @"MCare"
#define kAlertBtnOK                                 @"OK"    
#define kRequestTimeOutMessage          @"We can't establish a connection to MCare servers. Please make sure that your device is connected to the internet and try again"

#define kAlertServiceFailed								@"Failed. Try again"

#pragma mark - Common keys

#define APP_DELEGATE (AppDelegate*)[[UIApplication sharedApplication]delegate]

#define kBaseURL											@""

//=========================================================================================================================
#pragma mark - constant values
#define kDeviceId                       @"deviceId"
#define kDeviceType                  @"deviceType"
#define kDevice                          @"1"
#define kSessionToken                @"sessionToken"
#define KPost                             @"POST"
#define KGet                              @"GET"
#define KPut                               @"PUT"
#define kUsers								@"users"
#define kParameter                     @"Parameter"
#define kDataDic                        @"DataDic"
#define kBodyStr                        @"BodyStr"
#define kUserId                          @"userId"
#define kMyUpload                    @"myUpload"
#define kProfilePhoto                 @"profilePhoto"
#define kBoundry                       @"Boundry"
#define kServerUserNameId      @"userNameId"
#define kOption                         @"option"
#define kFileType                       @"fileType"
#define kServerImageURL         @"serverUrl"
#define kStatus                           @"status"
#define kMessage                        @"message"
//=========================================================================================================================
#pragma  mark - API keys
//=========================================================================================================================
#pragma mark - Sign Up Service
#define kURLSignup                   @"signup"
//#define kSocialMediaId               @"socialMediaId"
//#define kEmail                            @"email"
#define kPassword                      @"password"
//#define kDob                              @"dob"
//#define kGender                         @"gender"
//#define kAddress                        @"address"
#define kFullName                     @"fullName"
#define kProfileImage					@"profileImage"
#define kProfilePic						@"profilePic"
//#define kChemistLogo               @"chemistLogo"
//#define kPhone                          @"phone"
//#define kUserType                     @"userType"
//#define kLatitude                       @"latitude"
//#define kLongitude                    @"longitude"
#define kMobile							@"mobile"
#define kSignupFrom					@"signupFrom"

#define kCode								@"code"
#define kURLCheckCode				@"checkCode"
#define kURLUpdateDetails			@"updateDetails"
#define kUserName						@"userName"
#define kCodeStatus						@"codeStatus"
//	=========================================================================================================================
#pragma mark - Login Service

#define kLoginURL                   @"login"


//=========================================================================================================================//
#pragma mark - Logout Service

#define kLogout                         @"logout"

//=========================================================================================================================//
#pragma mark - Forget password Service


#define kForgetPwd                  @"forgetPwd"

//=========================================================================================================================//
#pragma mark - Fonts

#define kHelveticaNeue				@"HelveticaNeue-Roman"
//==========================================================================================================================//
#pragma mark - PATTERN CONSTANT

#define kPatternPhoneNumber        @"^\\d{9,12}$"
// =========================================================================================================================//






