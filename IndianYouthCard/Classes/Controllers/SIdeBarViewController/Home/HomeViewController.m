//
//  HomeViewController.m
//  IndianYouthCard
//
//  Created by Arbab Khan on 02/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	appDel = APP_DELEGATE;
//	[self setNavigationBar];
	
	UIGraphicsBeginImageContext(appDel.window.bounds.size);
	[appDel.window.layer drawInContext:UIGraphicsGetCurrentContext()];
	[AppManager sharedManager].imgBlur = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	self.sideBar = [AppManager createLeftBarWithDelegate:self];
	
	[slideShow slidesWithImages:[self getImages]];
	[slideShow setDelayInTransition:2.0f];
	[slideShow setDelegate:self];
	[slideShow setSlideShowMode:NHSlideShowModeFade];
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
	[self.navigationController setNavigationBarHidden:YES];
}
-(void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[slideShow doneLayout];
	[slideShow start];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -- Navigation bar Methods
- (void)setNavigationBar
{
	self.navigationController.navigationBarHidden = NO;
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	
	CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
	UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
	_headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
	_headerTitleSubtitleView.autoresizesSubviews = NO;
	
	CGRect titleFrame = CGRectMake(0,0, 150, 44);
	UILabel* titleView = [[UILabel alloc] initWithFrame:titleFrame];
	titleView.backgroundColor = [UIColor clearColor];
//	titleView.font = [UIFont fontWithName:kRobotoRegular size:15];
	titleView.textAlignment = NSTextAlignmentCenter;
	titleView.textColor = [UIColor whiteColor];
	titleView.text = @"Home";
	titleView.adjustsFontSizeToFitWidth = YES;
	[_headerTitleSubtitleView addSubview:titleView];
	self.navigationItem.titleView = _headerTitleSubtitleView;
	
	
	UIButton *sideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
	sideMenu.bounds = CGRectMake( 0, 0, 30, 30 );
	[sideMenu setImage:[UIImage imageNamed:@"menuIcon.png"] forState:UIControlStateNormal];
	[sideMenu addTarget:self action:@selector(SideMenuClicked) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *btnHome = [[UIBarButtonItem alloc] initWithCustomView:sideMenu];
	
	
	NSArray *arrBtnsLeft = [[NSArray alloc]initWithObjects:btnHome, nil];
	self.navigationItem.leftBarButtonItems = arrBtnsLeft;
	
}
- (IBAction)sideMenuClicked:(id)sender
{
	[self.sideBar show];
}
- (void)sideBarDelegatePushMethod:(UIViewController*)viewC{
	
	NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
	[navigationArray removeObjectAtIndex: self.navigationController.viewControllers.count -1];
	self.navigationController.viewControllers = navigationArray;
	[self.navigationController pushViewController:viewC animated:NO];
}

#pragma mark - NHSlideShow delegate functions


-(NSInteger)slideShowShouldStartFromSlide:(NHSlideShow *)slideShow
{
	return 0; // Pass the starting slide number
}

-(void)slideShow:(NHSlideShow *)slideShow didChangedSlideAtIndex:(NSInteger)slideIndex
{
	// TODO: You can display your image description using this delegate.
	NSLog(@"didChangedSlideAtIndex : %ld", (long)slideIndex);
}
#pragma mark - Helper functions

-(NSArray *)getImages
{
	NSMutableArray *mArr = [[NSMutableArray alloc] init];
	
	[mArr addObject:[UIImage imageNamed:@"banner1"]];
	[mArr addObject:[UIImage imageNamed:@"banner2"]];
	[mArr addObject:[UIImage imageNamed:@"banner3"]];
//	[mArr addObject:[UIImage imageNamed:@"3.png"]];
//	
//	[mArr addObject:[UIImage imageNamed:@"4.png"]];
//	[mArr addObject:[UIImage imageNamed:@"5.png"]];
//	[mArr addObject:[UIImage imageNamed:@"6.png"]];
//	
//	[mArr addObject:[UIImage imageNamed:@"7.png"]];
//	[mArr addObject:[UIImage imageNamed:@"8.png"]];
//	[mArr addObject:[UIImage imageNamed:@"9.png"]];
//	
//	[mArr addObject:[UIImage imageNamed:@"10.png"]];
	
	return mArr;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
