//
//  HomeViewController.h
//  IndianYouthCard
//
//  Created by Arbab Khan on 02/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NHSlideShow.h"
@interface HomeViewController : UIViewController<CDRTranslucentSideBarDelegate,NHSlideShowDelegate>
{
	AppDelegate *appDel;
	IBOutlet NHSlideShow *slideShow;
}
- (IBAction)sideMenuClicked:(id)sender;
@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;
@end
