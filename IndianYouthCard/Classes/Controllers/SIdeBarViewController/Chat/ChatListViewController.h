//
//  ChatListViewController.h
//  IndianYouthCard
//
//  Created by Arbab Khan on 02/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListViewController : UIViewController<CDRTranslucentSideBarDelegate>
{
	
}
@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;
- (IBAction)btnChat:(id)sender;

@end
