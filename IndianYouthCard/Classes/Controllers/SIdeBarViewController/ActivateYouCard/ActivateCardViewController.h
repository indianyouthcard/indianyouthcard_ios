//
//  ActivateCardViewController.h
//  IndianYouthCard
//
//  Created by Arbab Khan on 03/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivateCardViewController : UIViewController<CDRTranslucentSideBarDelegate>
{
	
}
@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;

@end
