//
//  ActivateCardViewController.m
//  IndianYouthCard
//
//  Created by Arbab Khan on 03/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import "ActivateCardViewController.h"

@interface ActivateCardViewController ()

@end

@implementation ActivateCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
	[self setNavigationBar];
	self.sideBar = [AppManager createLeftBarWithDelegate:self];
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
	[self.navigationController setNavigationBarHidden:NO];
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
#pragma mark -- Navigation bar Methods
- (void)setNavigationBar
{
	self.navigationController.navigationBarHidden = NO;
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	
	CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
	UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
	_headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
	_headerTitleSubtitleView.autoresizesSubviews = NO;
	
	CGRect titleFrame = CGRectMake(0,0, 150, 44);
	UILabel* titleView = [[UILabel alloc] initWithFrame:titleFrame];
	titleView.backgroundColor = [UIColor clearColor];
	//	titleView.font = [UIFont fontWithName:kRobotoRegular size:15];
	titleView.textAlignment = NSTextAlignmentCenter;
	titleView.textColor = [UIColor whiteColor];
	titleView.text = @"Activate your Card";
	titleView.adjustsFontSizeToFitWidth = YES;
	[_headerTitleSubtitleView addSubview:titleView];
	self.navigationItem.titleView = _headerTitleSubtitleView;
	
	
	UIButton *sideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
	sideMenu.bounds = CGRectMake( 0, 0, 30, 30 );
	[sideMenu setImage:[UIImage imageNamed:@"menuIcon.png"] forState:UIControlStateNormal];
	[sideMenu addTarget:self action:@selector(SideMenuClicked) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *btnHome = [[UIBarButtonItem alloc] initWithCustomView:sideMenu];
	
	
	NSArray *arrBtnsLeft = [[NSArray alloc]initWithObjects:btnHome, nil];
	self.navigationItem.leftBarButtonItems = arrBtnsLeft;
	
}
- (void)SideMenuClicked
{
	
	
	[self.sideBar show];
}
- (void)sideBarDelegatePushMethod:(UIViewController*)viewC{
	
	NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
	[navigationArray removeObjectAtIndex: self.navigationController.viewControllers.count -1];
	self.navigationController.viewControllers = navigationArray;
	[self.navigationController pushViewController:viewC animated:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
