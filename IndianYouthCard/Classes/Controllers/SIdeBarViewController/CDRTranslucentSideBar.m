
//
//  CDRTranslucentSideBar.m
//  AaramShop
//
//  Created by AppRoutes on 06/04/15.
//  Copyright (c) 2015 AppRoutes. All rights reserved.



#import "CDRTranslucentSideBar.h"
#import "HomeViewController.h"
#import "ActivateCardViewController.h"
#import "LocateRecStoreViewController.h"
#import "ChatListViewController.h"
#import "CheckBalanceViewController.h"
#import "RechargeViewController.h"
#import "TransferMoneyViewController.h"
#import "FriendsViewController.h"
#import "DiscountOffersViewController.h"
#import "UIImageEffects.h"



#define kDefaultHeaderFrame CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height)


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface CDRTranslucentSideBar ()
{
	NSMutableArray *arrMenu;
	NSMutableArray *arrOptions;
	NSMutableArray *arrImages;
	UITableView *tblView;
	UIStoryboard *storyboard;
	UIImageView * bluredImageView;
	UIView *secView;
	UINavigationController * navController;
	UIImage *effectImage;
	
}
@property (nonatomic, strong) UIToolbar *translucentView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;

@property (nonatomic, strong) CDRTranslucentSideBar *rightSideBar;

@property CGPoint panStartPoint;

@end

@implementation CDRTranslucentSideBar
- (id)init
{
	self = [super init];
	if (self) {
		
		[self initCDRTranslucentSideBar];
	}
	return self;
}

- (instancetype)initWithDirectionFromRight:(BOOL)showFromRight {
	self = [super init];
	if (self) {
		_showFromRight = showFromRight;
		[self initCDRTranslucentSideBar];
	}
	return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[tblView reloadData];
}
#pragma mark - Custom Initializer
- (void)initCDRTranslucentSideBar
{
	_hasShown = NO;
	isExist = NO;
	self.isCurrentPanGestureTarget = NO;
	
	self.animationDuration = 0.35f;
	
	[self initTranslucentView];
	
	self.view.backgroundColor = [UIColor clearColor];
	self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
	self.tapGestureRecognizer.delegate = self;
	[self.view addGestureRecognizer:self.tapGestureRecognizer];
	self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
	self.panGestureRecognizer.minimumNumberOfTouches = 1;
	self.panGestureRecognizer.maximumNumberOfTouches = 1;
	[self.view addGestureRecognizer:self.panGestureRecognizer];
}

- (void)initTranslucentView
{
	if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
		CGRect translucentFrame =
		CGRectMake(self.showFromRight ? self.view.bounds.size.width : -self.sideBarWidth, 0, self.sideBarWidth, self.view.bounds.size.height);
		self.translucentView = [[UIToolbar alloc] initWithFrame:translucentFrame];
		self.translucentView.frame = translucentFrame;
		self.translucentView.contentMode = _showFromRight ? UIViewContentModeTopRight : UIViewContentModeTopLeft;
		self.translucentView.clipsToBounds = YES;
		self.translucentView.backgroundColor=[UIColor clearColor];
		//        self.translucentView.barStyle =UIBarStyleDefault;
		
		[self.view.layer insertSublayer:self.translucentView.layer atIndex:0];
	}
}
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	appDel = APP_DELEGATE;
	arrMenu=[[NSMutableArray alloc]initWithObjects:@"Home",/*@"Activate your Card",*/@"Instant Reload",@"Chat",/*@"Check Card Balance",*/@"Recharge",@"Transfer Money",@"Friends",@"Discount / Offers",nil];
	arrOptions = [[NSMutableArray alloc]init];
	
	
	arrImages=[[NSMutableArray alloc]initWithObjects:@"menuHomeIcon",/*@"menuActiveCradNoIcon",*/@"menuLocateRechargeStoreIconInactive",@"menuChatIcon",/*@"menuCheckCardBalanceIcon",*/@"menuRechargeIcon",@"menuTransformMoneyIcon",@"menuFriendsIcon",@"menuLocateDiscountOfferIcon",nil];
	
	
	// Add PanGesture to Show SideBar by PanGesture
	UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
	[self.view addGestureRecognizer:panGestureRecognizer];
	
	
	
	// Create Content of SideBar
	UIView *vContentView = [[UIView alloc] initWithFrame:CGRectZero];
	vContentView.backgroundColor = [UIColor clearColor];
	if (!_showFromRight) {
		
		tblView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,260, [UIScreen mainScreen].bounds.size.height) style:UITableViewStyleGrouped];
		
		tblView.backgroundColor=[UIColor whiteColor];
		[vContentView addSubview:tblView];
		tblView.dataSource = self;
		tblView.delegate = self;
		[tblView setSeparatorColor:[UIColor colorWithRed:86/255.0f green:86/255.0f blue:86/255.0f alpha:1.0f]];
		tblView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
		tblView.scrollEnabled=YES;
		tblView.alwaysBounceVertical=YES;
		tblView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
	}
	[self setContentViewInSideBar:vContentView];
}
-(void)backButtonAction
{
	[self dismiss];
}
- (void)updateViewConstraints
{
	[super updateViewConstraints];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tblView
														  attribute:NSLayoutAttributeTop
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeTop
														 multiplier:1.0
														   constant:0.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tblView
														  attribute:NSLayoutAttributeLeading
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeLeading
														 multiplier:1.0
														   constant:0.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tblView
														  attribute:NSLayoutAttributeBottom
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeBottom
														 multiplier:1.0
														   constant:0.0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tblView
														  attribute:NSLayoutAttributeTrailing
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeTrailing
														 multiplier:1.0
														   constant:0.0]];
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	if (self.tag==0) {
		return arrMenu.count;
	}
	else if (self.tag==1) {
		return arrOptions.count;
	}
	return 1;
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (self.tag == 0) {
		return 176;
	}
	else
		return 0;
	return 0;
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 56;
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (self.tag == 0) {
		
		
		secView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tblView.frame.size.width, 176)];
		UIImageView *imgBackground=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, secView.frame.size.width, 176)];
		UIImageView *imgProfile=[[UIImageView alloc]initWithFrame:CGRectMake((secView.frame.size.width - 90)/2, 30, 90, 90)];
		        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
		        imgProfile.layer.masksToBounds=YES;
		imgProfile.image=[UIImage imageNamed:@"userPic1"];
		//        [imgProfile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kImage_url_100],[[NSUserDefaults standardUserDefaults] valueForKey:kProfileImage]]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
		//            if (image) {
		effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:imgProfile.image];
		imgBackground.image = effectImage;
		//            }
		//        }];
		
		
		
		
		UILabel *lblSeperator = [[UILabel alloc]initWithFrame:CGRectMake(0, secView.frame.size.height - 35, secView.frame.size.width, 35)];
		lblSeperator.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.40];
		
		UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, lblSeperator.frame.origin.y + 7, tblView.frame.size.width, 21)];
		lblName.textColor= [UIColor whiteColor];
		lblName.textAlignment=NSTextAlignmentCenter;
		//        lblName.font=[UIFont fontWithName:kRobotoBold size:15];
		lblName.text=@"Kate Watson";
		
		[secView addSubview:imgBackground];
		[secView addSubview:imgProfile];
		
		[secView addSubview:lblSeperator];
  
		[secView addSubview:lblName];
		
		
		//        [secView addSubview:btnEdit];
		
		
		return secView;
	}
	else
		return nil;
	return nil;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
	if (aCell == nil) {
		aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
		aCell.backgroundColor = [UIColor colorWithRed:222/255.0f green:215/255.0f blue:215/255.0f alpha:1.0f];
		aCell.selectionStyle = UITableViewCellSelectionStyleNone;
	}
	
	aCell.textLabel.text=[arrMenu objectAtIndex:indexPath.row];
	aCell.imageView.image=[UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
	aCell.textLabel.textColor=[UIColor colorWithRed:55/255.0 green:55/255.0 blue:55/255.0 alpha:1.0];
	//        aCell.textLabel.font=[UIFont fontWithName:kRobotoRegular size:16];
	
	return aCell;
	
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
	
	if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
		[tableView setSeparatorInset:UIEdgeInsetsZero];
	}
	
	if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
		[tableView setLayoutMargins:UIEdgeInsetsZero];
	}
	
	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
	isExist = NO;
	NSInteger count = self.navigationController.viewControllers.count;
	NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
	storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	[self dismiss];
	
	//    if (self.tag == 0) {
	//
	//
	//
	switch (indexPath.row) {
		case eHome:
		{
			
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[HomeViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}
					
				}
			}
			if (isExist == NO) {
				HomeViewController *homeVCon = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:homeVCon];
				}
				
			}
			
			
		}
			break;
//		case eActivateYourCard:
//		{
//			for (UIViewController *vc in navigationArray) {
//				if ([vc isKindOfClass:[ActivateCardViewController class]]) {
//					isExist = YES;
//					if (count > 2) {
//						[self.navigationController popToViewController:vc animated:NO];
//					}
//
//				}
//			}
//			if (isExist == NO) {
//				ActivateCardViewController *activateVCon = [storyboard instantiateViewControllerWithIdentifier:@"ActivateCardViewScene"];
//				
//				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
//					[self.delegate sideBarDelegatePushMethod:activateVCon];
//				}
//			}
//		}
//			break;
		case eInstantReload:
		{
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[LocateRecStoreViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}

				}
			}
			if (isExist == NO) {
				LocateRecStoreViewController *locateRectStoreVCon = [storyboard instantiateViewControllerWithIdentifier:@"LocateRecViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:locateRectStoreVCon];
				}
			}
			
		}
			break;
		case eChat:
		{
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[ChatListViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}
				}
			}
			if (isExist == NO) {
				ChatListViewController *chatListVCon = [storyboard instantiateViewControllerWithIdentifier:@"ChatListViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:chatListVCon];
				}
			}
			
		}
			break;
//		case eCheckCardBalance:
//		{
//			for (UIViewController *vc in navigationArray) {
//				if ([vc isKindOfClass:[CheckBalanceViewController class]]) {
//					isExist = YES;
//					if (count > 2) {
//						[self.navigationController popToViewController:vc animated:NO];
//					}
//				}
//			}
//			if (isExist == NO) {
//				CheckBalanceViewController *checkBalVCon = [storyboard instantiateViewControllerWithIdentifier:@"CheckBalanceViewScene"];
//				
//				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
//					[self.delegate sideBarDelegatePushMethod:checkBalVCon];
//				}
//			}
//			
//		}
//			break;
		case eRecharge:
		{
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[RechargeViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}
				}
			}
			if (isExist == NO) {
				RechargeViewController *rechargeVCon = [storyboard instantiateViewControllerWithIdentifier:@"RechargeViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:rechargeVCon];
				}
			}
			
		}
			break;
		case eTransferMoney:
		{
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[TransferMoneyViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}
				}
			}
			if (isExist == NO) {
				TransferMoneyViewController *transferVCon = [storyboard instantiateViewControllerWithIdentifier:@"TransferMoneyViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:transferVCon];
				}
			}
			
		}
			break;
		case eFriends:
		{
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[FriendsViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}
				}
			}
			if (isExist == NO) {
				FriendsViewController *friendVCon = [storyboard instantiateViewControllerWithIdentifier:@"FriendsViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:friendVCon];
				}
			}
			
		}
			break;
		case eDiscountOffers:
		{
			for (UIViewController *vc in navigationArray) {
				if ([vc isKindOfClass:[DiscountOffersViewController class]]) {
					isExist = YES;
					if (count > 2) {
						[self.navigationController popToViewController:vc animated:NO];
					}
				}
			}
			if (isExist == NO) {
				DiscountOffersViewController *discountOfferVCon = [storyboard instantiateViewControllerWithIdentifier:@"DiscountOffersViewScene"];
				
				if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)]) {
					[self.delegate sideBarDelegatePushMethod:discountOfferVCon];
				}
			}
		}
			break;
		default:
			break;
	}
	//
	
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex==1)
	{
		[self logout];
	}
}
-(void)logout
{
	[AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
	NSMutableDictionary *dict = [Utils setPredefindValueForWebservice];
	//    [dict setObject:@"7209" forKey:kAaramshopId];
	[dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] forKey:kUserId];
	
	[self performSelector:@selector(callWebServiceToLogout:) withObject:dict afterDelay:0.1];
}

- (void)callWebServiceToLogout:(NSMutableDictionary *)aDict
{
	if (![Utils isInternetAvailable])
	{
		//        [Utils stopActivityIndicatorInView:self.view];
		[AppManager stopStatusbarActivityIndicator];
		//		[Utils showAlertView:kAlertTitle message:kAlertCheckInternetConnection delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
		return;
	}
	//	[aaramShop_ConnectionManager getDataForFunction:kURLLogout withInput:aDict withCurrentTask:TASK_TO_LOGOUT andDelegate:self ];
}
/*- (void)responseReceived:(id)responseObject
 {
	[AppManager stopStatusbarActivityIndicator];
	if(aaramShop_ConnectionManager.currentTask == TASK_TO_LOGOUT)
	{
 if([[responseObject objectForKey:kstatus] intValue] == 1)
 {
 [gCXMPPController disconnect];
 [AppManager removeDataFromNSUserDefaults];
 appDel.myCurrentLocation = nil;
 gAppManager.intCount = 0;
 [AppManager saveCountOfProductsInCart:gAppManager.intCount];
 
 [[NSNotificationCenter defaultCenter] postNotificationName:kLogoutSuccessfulNotificationName object:self userInfo:nil];
 [Utils showAlertView:kAlertTitle message:[responseObject objectForKey:kMessage] delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
 }
	}
 }
 - (void)didFailWithError:(NSError *)error
 {
	//    [Utils stopActivityIndicatorInView:self.view];
	[AppManager stopStatusbarActivityIndicator];
	[aaramShop_ConnectionManager failureBlockCalled:error];
 }*/

//	[Utils showAlertView:kAlertTitle message:@"Logout successfully" delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];

-(void)EditAddress
{
	/*  LocationEnterViewController *locationScreen = (LocationEnterViewController*) [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationEnterScreen"];
	 [self presentViewController:locationScreen animated:YES completion:nil];*/
}
-(void)btnAppliedClicked
{
	//    [self dismiss];
	//    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	//    AppliedJobsViewController *Vc=[storyboard instantiateViewControllerWithIdentifier:@"jobAppliedVC"];
	//    if ([self.delegate respondsToSelector:@selector(sideBarDelegatePushMethod:)])
	//    {
	//        [self.delegate sideBarDelegatePushMethod:Vc];
	//    }
	
}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (void)loadView
{
	[super loadView];
}

#pragma mark - Layout
- (BOOL)shouldAutorotate
{
	return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
	
	if ([self isViewLoaded] && self.view.window != nil) {
		[self layoutSubviews];
	}
}

- (void)layoutSubviews
{
	CGFloat x = self.showFromRight ? self.parentViewController.view.bounds.size.width - self.sideBarWidth : 0;
	
	if (self.contentView != nil) {
		self.contentView.frame = CGRectMake(x, 0, self.sideBarWidth, self.parentViewController.view.bounds.size.height);
	}
}

#pragma mark - Accessor
- (void)setTranslucentStyle:(UIBarStyle)translucentStyle
{
	self.translucentView.barStyle = translucentStyle;
}

- (UIBarStyle)translucentStyle
{
	return self.translucentView.barStyle;
}

- (void)setTranslucent:(BOOL)translucent
{
	self.translucentView.translucent = translucent;
}

- (BOOL)translucent
{
	return self.translucentView.translucent;
}

- (void)setTranslucentAlpha:(CGFloat)translucentAlpha
{
	self.translucentView.alpha = translucentAlpha;
}

- (CGFloat)translucentAlpha
{
	return self.translucentView.alpha;
}

- (void)setTranslucentTintColor:(UIColor *)translucentTintColor
{
	self.translucentView.tintColor = translucentTintColor;
}

- (UIColor *)translucentTintColor
{
	return self.translucentView.tintColor;
}


#pragma mark - Show
- (void)showInViewController:(UIViewController *)controller animated:(BOOL)animated
{
	if ([self.delegate respondsToSelector:@selector(sideBar:willAppear:)]) {
		[self.delegate sideBar:self willAppear:animated];
	}
	
	[self addToParentViewController:controller callingAppearanceMethods:YES];
	self.view.frame = controller.view.bounds;
	
	CGFloat parentWidth = self.view.bounds.size.width;
	CGRect sideBarFrame = self.view.bounds;
	sideBarFrame.origin.x = self.showFromRight ? parentWidth : -self.sideBarWidth;
	sideBarFrame.size.width = self.sideBarWidth;
	
	if (self.contentView != nil) {
		self.contentView.frame = sideBarFrame;
	}
	sideBarFrame.origin.x = self.showFromRight ? parentWidth - self.sideBarWidth : 0;
	
	void (^animations)() = ^{
		if (self.contentView != nil) {
			self.contentView.frame = sideBarFrame;
		}
		self.translucentView.frame = sideBarFrame;
	};
	void (^completion)(BOOL) = ^(BOOL finished)
	{
		_hasShown = YES;
		self.isCurrentPanGestureTarget = YES;
		if (finished && [self.delegate respondsToSelector:@selector(sideBar:didAppear:)]) {
			// [self.delegate sideBar:self didAppear:animated];
		}
	};
	
	if (animated) {
		[UIView animateWithDuration:self.animationDuration delay:0 options:kNilOptions animations:animations completion:completion];
	} else {
		animations();
		completion(YES);
	}
}

- (void)showAnimated:(BOOL)animated
{
	UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
	while (controller.presentedViewController != nil) {
		controller = controller.presentedViewController;
	}
	[self showInViewController:controller animated:animated];
}

- (void)show
{
	[self showAnimated:YES];
}

#pragma mark - Show by Pangesture
- (void)startShow:(CGFloat)startX
{
	UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
	while (controller.presentedViewController != nil) {
		controller = controller.presentedViewController;
	}
	[self addToParentViewController:controller callingAppearanceMethods:YES];
	self.view.frame = controller.view.bounds;
	
	CGFloat parentWidth = self.view.bounds.size.width;
	
	CGRect sideBarFrame = self.view.bounds;
	sideBarFrame.origin.x = self.showFromRight ? parentWidth : -self.sideBarWidth;
	sideBarFrame.size.width = self.sideBarWidth;
	if (self.contentView != nil) {
		self.contentView.frame = sideBarFrame;
	}
	self.translucentView.frame = sideBarFrame;
}

- (void)move:(CGFloat)deltaFromStartX
{
	
	CGRect sideBarFrame = self.translucentView.frame;
	CGFloat parentWidth = self.view.bounds.size.width;
	
	if (self.showFromRight) {
		CGFloat x = deltaFromStartX;
		if (deltaFromStartX >= self.sideBarWidth) {
			x = self.sideBarWidth;
		}
		sideBarFrame.origin.x = parentWidth - x;
	} else {
		CGFloat x = deltaFromStartX - _sideBarWidth;
		if (x >= 0) {
			x = 0;
		}
		sideBarFrame.origin.x = x;
	}
	
	if (self.contentView != nil) {
		self.contentView.frame = sideBarFrame;
	}
	self.translucentView.frame = sideBarFrame;
}

- (void)showAnimatedFrom:(BOOL)animated deltaX:(CGFloat)deltaXFromStartXToEndX
{
	if ([self.delegate respondsToSelector:@selector(sideBar:willAppear:)]) {
		[self.delegate sideBar:self willAppear:animated];
	}
	
	CGRect sideBarFrame = self.translucentView.frame;
	CGFloat parentWidth = self.view.bounds.size.width;
	
	sideBarFrame.origin.x = self.showFromRight ? parentWidth - sideBarFrame.size.width : 0;
	
	void (^animations)() = ^{
		if (self.contentView != nil) {
			self.contentView.frame = sideBarFrame;
		}
		
		self.translucentView.frame = sideBarFrame;
	};
	void (^completion)(BOOL) = ^(BOOL finished)
	{
		_hasShown = YES;
		if (finished && [self.delegate respondsToSelector:@selector(sideBar:didAppear:)]) {
			[self.delegate sideBar:self didAppear:animated];
		}
	};
	
	if (animated) {
		[UIView animateWithDuration:self.animationDuration delay:0 options:kNilOptions animations:animations completion:completion];
	} else {
		animations();
		completion(YES);
	}
}

#pragma mark - Dismiss
- (void)dismiss
{
	[self dismissAnimated:YES];
}

- (void)dismissAnimated:(BOOL)animated
{
	if ([self.delegate respondsToSelector:@selector(sideBar:willDisappear:)]) {
		[self.delegate sideBar:self willDisappear:animated];
	}
	
	void (^completion)(BOOL) = ^(BOOL finished)
	{
		[self removeFromParentViewControllerCallingAppearanceMethods:YES];
		_hasShown = NO;
		self.isCurrentPanGestureTarget = NO;
		if ([self.delegate respondsToSelector:@selector(sideBar:didDisappear:)]) {
			[self.delegate sideBar:self didDisappear:animated];
		}
	};
	
	if (animated) {
		CGRect sideBarFrame = self.translucentView.frame;
		CGFloat parentWidth = self.view.bounds.size.width;
		sideBarFrame.origin.x = self.showFromRight ? parentWidth : -self.sideBarWidth;
		[UIView animateWithDuration:self.animationDuration
							  delay:0
							options:UIViewAnimationOptionBeginFromCurrentState
						 animations:^{
							 if (self.contentView != nil) {
								 self.contentView.frame = sideBarFrame;
							 }
							 self.translucentView.frame = sideBarFrame;
						 }
						 completion:completion];
	} else {
		completion(YES);
	}
}

#pragma mark - Dismiss by Pangesture
- (void)dismissAnimated:(BOOL)animated deltaX:(CGFloat)deltaXFromStartXToEndX
{
	if ([self.delegate respondsToSelector:@selector(sideBar:willDisappear:)]) {
		[self.delegate sideBar:self willDisappear:animated];
	}
	
	void (^completion)(BOOL) = ^(BOOL finished)
	{
		[self removeFromParentViewControllerCallingAppearanceMethods:YES];
		_hasShown = NO;
		self.isCurrentPanGestureTarget = NO;
		if ([self.delegate respondsToSelector:@selector(sideBar:didDisappear:)]) {
			[self.delegate sideBar:self didDisappear:animated];
		}
	};
	
	if (animated) {
		CGRect sideBarFrame = self.translucentView.frame;
		CGFloat parentWidth = self.view.bounds.size.width;
		sideBarFrame.origin.x = self.showFromRight ? parentWidth : -self.sideBarWidth + deltaXFromStartXToEndX;
		
		[UIView animateWithDuration:self.animationDuration
							  delay:0
							options:UIViewAnimationOptionBeginFromCurrentState
						 animations:^{
							 if (self.contentView != nil) {
								 self.contentView.frame = sideBarFrame;
							 }
							 self.translucentView.frame = sideBarFrame;
						 }
						 completion:completion];
	} else {
		completion(YES);
	}
}

#pragma mark - Gesture Handler
- (void)handleTapGesture:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.view];
	if (!CGRectContainsPoint(self.translucentView.frame, location)) {
		[self dismissAnimated:YES];
	}
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)recognizer
{
	if (!self.isCurrentPanGestureTarget) {
		return;
	}
	
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		self.panStartPoint = [recognizer locationInView:self.view];
	}
	
	if (recognizer.state == UIGestureRecognizerStateChanged) {
		CGPoint currentPoint = [recognizer locationInView:self.view];
		if (!self.showFromRight) {
			[self move:self.sideBarWidth + currentPoint.x - self.panStartPoint.x];
		} else {
			[self move:self.sideBarWidth + self.panStartPoint.x - currentPoint.x];
		}
	}
	
	if (recognizer.state == UIGestureRecognizerStateEnded) {
		CGPoint endPoint = [recognizer locationInView:self.view];
		
		if (!self.showFromRight) {
			if (self.panStartPoint.x - endPoint.x < self.sideBarWidth / 3) {
				[self showAnimatedFrom:YES deltaX:endPoint.x - self.panStartPoint.x];
			} else {
				[self dismissAnimated:YES deltaX:endPoint.x - self.panStartPoint.x];
			}
		} else {
			if (self.panStartPoint.x - endPoint.x >= self.sideBarWidth / 3) {
				[self showAnimatedFrom:YES deltaX:self.panStartPoint.x - endPoint.x];
			} else {
				[self dismissAnimated:YES deltaX:self.panStartPoint.x - endPoint.x];
			}
		}
	}
}

- (void)handlePanGestureToShow:(UIPanGestureRecognizer *)recognizer inView:(UIView *)parentView
{
	if (!self.isCurrentPanGestureTarget) {
		return;
	}
	
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		self.panStartPoint = [recognizer locationInView:parentView];
		[self startShow:self.panStartPoint.x];
	}
	
	if (recognizer.state == UIGestureRecognizerStateChanged) {
		CGPoint currentPoint = [recognizer locationInView:parentView];
		if (!self.showFromRight) {
			[self move:currentPoint.x - self.panStartPoint.x];
		} else {
			[self move:self.panStartPoint.x - currentPoint.x];
		}
	}
	
	if (recognizer.state == UIGestureRecognizerStateEnded) {
		CGPoint endPoint = [recognizer locationInView:parentView];
		
		if (!self.showFromRight) {
			if (endPoint.x - self.panStartPoint.x >= self.sideBarWidth / 3) {
				[self showAnimatedFrom:YES deltaX:endPoint.x - self.panStartPoint.x];
			} else {
				[self dismissAnimated:YES deltaX:endPoint.x - self.panStartPoint.x];
			}
		} else {
			if (self.panStartPoint.x - endPoint.x >= self.sideBarWidth / 3) {
				[self showAnimatedFrom:YES deltaX:self.panStartPoint.x - endPoint.x];
			} else {
				[self dismissAnimated:YES deltaX:self.panStartPoint.x - endPoint.x];
			}
		}
	}
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
	if (touch.view != gestureRecognizer.view) {
		return NO;
	}
	return YES;
}

#pragma mark - ContentView
- (void)setContentViewInSideBar:(UIView *)contentView
{
	if (self.contentView != nil) {
		[self.contentView removeFromSuperview];
	}
	
	self.contentView = contentView;
	self.contentView.backgroundColor = [UIColor clearColor];
	UIImageView *imgProfile=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
	imgProfile.image = [UIImage imageNamed:@"userPic1"];
	//	[imgProfile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kImage_url_100],[[NSUserDefaults standardUserDefaults] valueForKey:kProfileImage]]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
	//		if (image) {
	effectImage = [UIImageEffects imageByApplyingDarkEffectToImage: [AppManager sharedManager].imgBlur ];
//	effectImage = [UIImageEffects imageByApplyingDarkEffectToImage: [UIImage imageNamed:@"userPic1"]];
	imgProfile.image = effectImage;
	//		}
	////	}];
	//		if (blurView.blurRadius < 5)
	//		{
	//			[UIView animateWithDuration:0.5 animations:^{
	//				blurView.blurRadius = 40;
	//			}];
	//		}
	//		else
	//		{
	//			[UIView animateWithDuration:0.5 animations:^{
	//				blurView.blurRadius = 0;
	//			}];
	//		}
	
	//	[self.view addSubview:blurView];
	[self.view addSubview:imgProfile];
	[self.view setBackgroundColor:[UIColor clearColor]];
	[self.view addSubview:self.contentView];
	
	
}

#pragma mark - Helper
- (void)addToParentViewController:(UIViewController *)parentViewController callingAppearanceMethods:(BOOL)callAppearanceMethods
{
	if (self.parentViewController != nil) {
		[self removeFromParentViewControllerCallingAppearanceMethods:callAppearanceMethods];
	}
	
	if (callAppearanceMethods) [self beginAppearanceTransition:YES animated:NO];
	[parentViewController addChildViewController:self];
	[parentViewController.view addSubview:self.view];
	[self didMoveToParentViewController:self];
	if (callAppearanceMethods) [self endAppearanceTransition];
}

- (void)removeFromParentViewControllerCallingAppearanceMethods:(BOOL)callAppearanceMethods
{
	if (callAppearanceMethods) [self beginAppearanceTransition:NO animated:NO];
	[self willMoveToParentViewController:nil];
	[self.view removeFromSuperview];
	[self removeFromParentViewController];
	if (callAppearanceMethods) [self endAppearanceTransition];
}











@end
