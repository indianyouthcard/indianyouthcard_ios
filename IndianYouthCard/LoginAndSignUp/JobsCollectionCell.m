//
//  JobsCollectionCell.m
//  Jobick
//
//  Created by Approutes on 4/22/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import "JobsCollectionCell.h"
//#import "JobLocationModel.h"

@implementation JobsCollectionCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        imgBackground = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
        imgBackground.layer.masksToBounds = YES;
        [self.contentView addSubview:imgBackground];

    }
    return self;
}



-(void)updateCellWithData
{
    imgBackground.image = [UIImage imageNamed:[_arrImages objectAtIndex:_indexPath.row]];
    
}



@end
