//
//  LoginViewController.m
//  IndianYouthCard
//
//  Created by Approutes on 02/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = NO;
    
//    self.title = @"Profile";
//    [self.navigationItem setHidesBackButton:YES];

    
    
    
    [collectionview registerClass:JobsCollectionCell.class
           forCellWithReuseIdentifier:NSStringFromClass(JobsCollectionCell.class)];

    
    arrImages = [[NSMutableArray alloc]init];
    
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];
    [arrImages addObject:@"1.png"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - UICollectionView Delegate & DataSource Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    //-55 is a tweak value to remove top spacing
    return CGSizeZero;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize sizeCell=CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    return sizeCell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  arrImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JobsCollectionCell *cell = (JobsCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(JobsCollectionCell.class) forIndexPath:indexPath];
    
    cell.indexPath = indexPath;
    cell.arrImages = arrImages;
    
    [cell updateCellWithData];
    
    return cell;
    
}



- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (void)collectionView:(UICollectionView *)collectionViewSelected didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}



@end
