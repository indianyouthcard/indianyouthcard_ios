//
//  JobsCollectionCell.h
//  Jobick
//
//  Created by Approutes on 4/22/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobsCollectionCell : UICollectionViewCell
{
    UIImageView *imgBackground;
}
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSMutableArray *arrImages;

-(void)updateCellWithData;
@end
