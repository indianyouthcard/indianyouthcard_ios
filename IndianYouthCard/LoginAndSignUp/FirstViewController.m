//
//  ViewController.m
//  IndianYouthCard
//
//  Created by Approutes on 02/09/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
{
    MPMoviePlayerController *theMoviPlayer;
    UIView *blurView;
}
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    blurView = [[UIView alloc]initWithFrame:CGRectMake(0, -5, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height+10)];
    blurView.backgroundColor = [UIColor blackColor];
    blurView.alpha = 0.65f;
    UIToolbar* bgToolbar = [[UIToolbar alloc] initWithFrame:blurView.frame];
    bgToolbar.barStyle = UIBarStyleDefault;
    [blurView.superview insertSubview:bgToolbar belowSubview:blurView];
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *moviePath1 = [bundle pathForResource:@"demo" ofType:@"3gp"];
    NSURL *movieURL1 = [NSURL fileURLWithPath:moviePath1];
    NSArray *arrMovieURLs = [[NSArray alloc]initWithObjects:movieURL1, nil];
    
    theMoviPlayer = [[MPMoviePlayerController alloc] initWithContentURL:[arrMovieURLs objectAtIndex:0]];
    theMoviPlayer.controlStyle = MPMovieControlStyleNone;
    theMoviPlayer.repeatMode = MPMovieRepeatModeOne;
    theMoviPlayer.scalingMode = MPMovieScalingModeAspectFill;
    
    [theMoviPlayer.view setFrame:CGRectMake(0, -5, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height+10)];
    [subView addSubview:theMoviPlayer.view];
    [subView addSubview:blurView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;

    [theMoviPlayer play];
    
}

- (IBAction)actionLogin:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"LoginView" sender:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
